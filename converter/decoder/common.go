package decoder

import (
	"gitee.com/smartos-club/dsapid"
	"gitee.com/smartos-club/dsapid/converter"
	"gitee.com/smartos-club/dsapid/converter/dsapi"
	"gitee.com/smartos-club/dsapid/converter/imgapi"
	"gitee.com/smartos-club/dsapid/storage"
)

func DecodeToManifest(data dsapid.Table, provider dsapid.SyncProvider, users storage.UserStorage) *dsapid.ManifestResource {
	var decoder converter.ManifestDecoder

	if _, ok := data["v"]; ok {
		decoder, _ = imgapi.NewDecoder(provider, users)
	} else {
		decoder, _ = dsapi.NewDecoder(provider, users)
	}

	return decoder.Decode(data)
}
